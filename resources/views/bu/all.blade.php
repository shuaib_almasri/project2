@extends('admin/layout/layout')

@section('title')
    ﻛﻞ اﻟﻤﻨﺘﺠﺎﺕ
@endsection

@section('header')
   {!! Html::style('admin/cus/buall.css')!!}
   @endsection


@section('content')
    <div class="container">
        <div class="row profile">
            <div class="col-md-3" style="direction: rtl">
                <div class="profile-sidebar ">

                    <!-- SIDEBAR USER TITLE -->
                    <div class="profile-usertitle">
                        <div class="profile-usertitle-name">
                            Marcus Doe
                        </div>
                        <div class="profile-usertitle-job">
                            Developer
                        </div>
                    </div>
                    <!-- END SIDEBAR USER TITLE -->
                    <!-- SIDEBAR BUTTONS -->
                    <div class="profile-userbuttons">
                        <button type="button" class="btn btn-success btn-sm">Follow</button>
                        <button type="button" class="btn btn-danger btn-sm">Message</button>
                    </div>
                    <!-- END SIDEBAR BUTTONS -->
                    <!-- SIDEBAR MENU -->
                    <div class="profile-usermenu">
                        <ul class="nav">
                            <li class="active">
                                <a href="/showallbulding">
                                    <i class="glyphicon glyphicon-home"></i>
                                    ﻛﻞ اﻟﻤﻨﺘﺠﺎﺕ </a>
                            </li>
                            <li>
                                <a href="/ForFood">
                                    <i class="glyphicon glyphicon-user"></i>
                                    اﻟﻤﻮاﺩ اﻟﻐﺬاﺋﻴﺔ </a>
                            </li>
                            <li>
                                <a href="/ForClear" >
                                    <i class="glyphicon glyphicon-user"></i>
                                    ﻣﻨﻈﻔﺎﺕ ﻣﻨﺰﻟﻴﺔ </a>
                            </li>
                            <li>
                                <a href="/ForFroit">
                                    <i class="glyphicon glyphicon-user"></i>
                                    ﻓﻮاﻛﻪ  </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="glyphicon glyphicon-flag"></i>
                                    ﻣﺴﺎﻋﺪﺓ </a>
                            </li>
                        </ul>
                    </div>
                    <!-- END MENU -->
                </div>
            </div>

            <div class="col-md-9">




                
                <div class="profile-content">
                    @include('website.bu.sharefile',['bu'=>$buAll])
                </div>
            </div>
        </div>
    </div>
    <br>
    <br>

@endsection
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://code.jquery.com/jquery.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->


{!! Html::script('admin/bootstrap/js/bootstrap.min.js')!!}
{!! Html::script('admin/js/jquery.min.js')!!}

<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
