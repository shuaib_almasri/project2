
@if(count($bu) >= 1)
        <div class="row">
    @foreach($bu as $b)
        <div class="col-md-3" style="margin-top: 20px">
            <article class="col-item">
                <div class="photo">
                    <div class="options-cart-round">
                        <button class="btn btn-default" title="Add to cart">
                            <span class="fa fa-shopping-cart"></span>
                        </button>
                    </div>
                    <a href="#"> <img src="website/image/m2.jpg" class="img-responsive" alt="Product Image" /> </a>
                </div>
                <div class="info">
                    <div class="row">
                        <div class="price-details col-md-6">
                            <p class="details">
                            @if($b['bu_type'] == 0)
                                <h3>ﻣﻮاﺩ ﻏﺬاﺋﻴﺔ</h3>
                            @elseif($b['bu_type'] == 1)
                                <h3> ﻓﻮاﻛﻪ</h3>
                            @elseif($b['bu_type'] == 2)
                                <h3> ﻣﻨﻈﻔﺎﺕ ﻣﻨﺰﻟﻴﺔ</h3>
                            @else
                                <h3></h3>
                                @endif
                            </p>
                            <h3>{{$b['bu_name']}}</h3>
                            <span class="price-new">{{$b['bu_price']}}</span>
                        </div>
                    </div>
                </div>
            </article>
        </div>

    @endforeach
        </div>
@else
<div class="alert alert-danger">
    ﻻﻳﻮﺟﺪ اﻱ ﻣﻨﺘﺠﺎﺕ
</div>

    @endif